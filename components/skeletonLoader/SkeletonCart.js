import React from 'react'
import ContentLoader from 'react-content-loader'
import Container from '@mui/material/Container'

const HeadBodyGrid = ({ ...rest }) => (
    <Container className="skeleton__multi">
    <ContentLoader
      viewBox="0 0 500 120"
      backgroundColor="#f3f3f3"
      foregroundColor="#ecebeb"
    >
      <rect x="0" y="0" rx="0" ry="0" width="125" height="80" />
      <rect x="0" y="90" rx="0" ry="0" width="125" height="15" /> 

      <rect x="130" y="0" rx="0" ry="0" width="125" height="80" />
      <rect x="130" y="90" rx="0" ry="0" width="125" height="15" /> 

      <rect x="260" y="0" rx="0" ry="0" width="125" height="80" />
      <rect x="260" y="90" rx="0" ry="0" width="125" height="15" /> 

      <rect x="390" y="0" rx="0" ry="0" width="125" height="80" />
      <rect x="390" y="90" rx="0" ry="0" width="125" height="15" /> 
    

    </ContentLoader>
  </Container>
)


export default HeadBodyGrid