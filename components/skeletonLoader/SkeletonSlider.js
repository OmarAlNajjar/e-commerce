import React from 'react'
import ContentLoader from 'react-content-loader'

const SkeletonSlider = ({ ...rest }) => (
    <>
    <ContentLoader
      viewBox="0 0 500 150"
      backgroundColor="#f3f3f3"
      foregroundColor="#ecebeb"
    >
      <rect x="0" y="0" rx="0" ry="0" width="1000" height="1000" />
    </ContentLoader>
  </>
)


export default SkeletonSlider