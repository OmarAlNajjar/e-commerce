import Head from "next/head";
import styles from "../../styles/Waves.module.scss";
import React, { useEffect, useRef, useState } from "react";
import NET from "vanta/dist/vanta.net.min";
import * as THREE from "three";
import { Container, CardActionArea } from "@mui/material";
import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";
import CardMedia from "@mui/material/CardMedia";
import Typography from "@mui/material/Typography";
import LocalShippingIcon from "@mui/icons-material/LocalShipping";
import DevicesOtherIcon from '@mui/icons-material/DevicesOther';




function CardServiceDev() {
  return (
    <Card sx={{ maxWidth: 345 }} className={styles.card}>
      <CardActionArea className={styles.showcase}>
        <DevicesOtherIcon />

        <CardContent>
          <Typography gutterBottom variant="h5" component="div" className={styles.headline}>
          best products
          </Typography>

        </CardContent>
      </CardActionArea>
    </Card>
  );
}



function CardServiceDel() {
  return (
    <Card sx={{ maxWidth: 345 }} className={styles.card}>
      <CardActionArea className={styles.showcase}>
        <LocalShippingIcon />

        <CardContent>
          <Typography gutterBottom variant="h5" component="div" className={styles.headline}>
          delivery
          </Typography>

        </CardContent>
      </CardActionArea>
    </Card>
  );
}



export default function Home() {
  const [vantaEffect, setVantaEffect] = useState(0);
  const vantaRef = useRef(null);
  useEffect(() => {
    if (!vantaEffect) {
      setVantaEffect(
        NET({
          el: vantaRef.current,
          THREE,
          points: 10,
          maxDistance: 20,
          spacing: 4,
          color: 0xffd000,
          backgroundColor: "#121212",
        })
      );
    }
  }, [vantaEffect]);
  return (
    <div className={styles.container}>
      <main className={styles.main} ref={vantaRef}>
          <CardServiceDev/>
          <CardServiceDel/>
      </main>
    </div>
  );
}
