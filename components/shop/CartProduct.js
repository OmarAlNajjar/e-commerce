import React, { useState } from "react";
import Card from "@mui/material/Card";
import CardActions from "@mui/material/CardActions";
import CardContent from "@mui/material/CardContent";
import CardMedia from "@mui/material/CardMedia";
import Button from "@mui/material/Button";
import Typography from "@mui/material/Typography";
import RemoveShoppingCartIcon from "@mui/icons-material/RemoveShoppingCart";
import FavoriteIcon from "@mui/icons-material/Favorite";
import VisibilityIcon from "@mui/icons-material/Visibility";
import ButtonGroup from "@mui/material/ButtonGroup";
import styles from "../../styles/Product.module.scss";
import Grid from "@mui/material/Grid";
import { connect } from "react-redux";
import {
  adjustQty,
  removeFromCart,
} from "../../redux/shopping/shoppingActions";

const CartProduct = ({
  qty,
  id,
  price,
  name,
  image,
  removeFromCart,
  adjustQty,
}) => {
  const [input, setInput] = useState(qty);

  const onChangeHandler = (e) => {
    setInput(e.target.value);
    adjustQty(id, e.target.value);
  };

  const onIncress = (e) => {
    setInput(e.target.value + 1 );
    adjustQty(id, e.target.value);


  }

  return (
    <Grid item xs={12} sm={6} md={6} lg={4}>
      <Card sx={{ maxWidth: 345 }} className={styles.productItem}>
        <CardMedia component="img" height="140" image={image} alt={name} />
        <CardContent>
          <Typography
            gutterBottom
            variant="h5"
            component="div"
            className={styles.details}
          >
            <div>{name}</div>|<div>{price}$</div>
          </Typography>
        </CardContent>
        <CardActions className={styles.actions}>
          <ButtonGroup variant="text" aria-label="text button group">
            <Button size="small" onClick={() => removeFromCart(id)}>
              <RemoveShoppingCartIcon />
            </Button>
            <Button size="small">
              <FavoriteIcon />
            </Button>
            <Button size="small">
              <VisibilityIcon />
            </Button>
          </ButtonGroup>
          <ButtonGroup variant="text" aria-label="text button group">

            <input
              min="1"
              type="number"
              id="qty"
              name="qty"
              className={styles.qty}
              value={input}
              onChange={onChangeHandler}
            />

          </ButtonGroup>
        </CardActions>
      </Card>
    </Grid>
  );
};

const mapDispatchToProps = (dispatch) => {
  return {
    adjustQty: (id, value) => dispatch(adjustQty(id, value)),
    removeFromCart: (id) => dispatch(removeFromCart(id)),
  };
};

export default connect(null, mapDispatchToProps)(CartProduct);
