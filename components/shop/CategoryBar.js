




import * as React from 'react';
import Tabs from '@mui/material/Tabs';
import Tab from '@mui/material/Tab';
import Box from '@mui/material/Box';
import LaptopIcon from '@mui/icons-material/Laptop';
import PhoneAndroidIcon from '@mui/icons-material/PhoneAndroid';
import MouseIcon from '@mui/icons-material/Mouse';
import KeyboardIcon from '@mui/icons-material/Keyboard';
import DevicesOtherIcon from '@mui/icons-material/DevicesOther';
import WatchIcon from '@mui/icons-material/Watch';
import styles from '../../styles/CategoryBar.module.scss'

export default function ScrollableTabsButtonForce() {
  const [value, setValue] = React.useState(0);

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  return (
    <Box className={styles.tab__container} sx={{ bgcolor: 'white' }}>
    <div className={styles.title}>chose category {"->"}  </div>
      <Tabs
        value={value}
        onChange={handleChange}
        variant="scrollable"
        scrollButtons
        allowScrollButtonsMobile
        aria-label="scrollable force tabs example"
      >
        <Tab icon={<DevicesOtherIcon/>} />
        <Tab icon={<LaptopIcon/>} />
        <Tab icon={<PhoneAndroidIcon/>} />
        <Tab icon={<MouseIcon/>} />
        <Tab icon={<KeyboardIcon/>} />
        <Tab icon={<WatchIcon/>} />
      </Tabs>
    </Box>
  );
}
