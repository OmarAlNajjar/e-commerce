import React, { useEffect, useState } from "react";
import Card from "@mui/material/Card";
import CardActions from "@mui/material/CardActions";
import CardContent from "@mui/material/CardContent";
import CardMedia from "@mui/material/CardMedia";
import Button from "@mui/material/Button";
import Typography from "@mui/material/Typography";
import ShoppingCartIcon from "@mui/icons-material/ShoppingCart";
import FavoriteIcon from "@mui/icons-material/Favorite";
import VisibilityIcon from "@mui/icons-material/Visibility";
import ButtonGroup from "@mui/material/ButtonGroup";
import styles from "../../styles/Product.module.scss";
import { connect } from "react-redux";
import { loadItem, addToCart } from "../../redux/shopping/shoppingActions";
import AOS from "aos";
import "aos/dist/aos.css"; // You can also use <link> for styles

const Product = ({ addToCart, animationDelay, product }) => {
  const [maxValue, setmaxValue] = useState(animationDelay.length);

  useEffect(() => {
    AOS.init({ duration: 1000 });
    setmaxValue(!maxValue ? maxValue : maxValue + 1);

    console.log(product, "herer");

    let products = document.getElementsByClassName(`${styles.productItem}`);

    console.log(animationDelay.length + 1, "here");

    for (let i = 1; i < maxValue; ++i) {
      products[i].setAttribute("data-aos-offset", `${i}00`);
      console.log(products[i].getAttribute("data-aos-offset"));
    }
  }, []);

  return (
    <Card
      sx={{ maxWidth: 345 }}
      className={styles.productItem}
      data-aos="fade-up"
    >
      <CardMedia
        component="img"
        height="140"
        image={product.image}
        alt={product.name}
      />
      <CardContent>
        <Typography
          gutterBottom
          variant="h5"
          component="div"
          className={styles.details}
        >
          <div>{product.name}</div>|<div>{product.price}$</div>
        </Typography>
      </CardContent>
      <CardActions className={styles.actions}>
        <ButtonGroup variant="text" aria-label="text button group">
          <Button size="small" onClick={() => addToCart(product, product.id)}>
            <ShoppingCartIcon />
          </Button>
          <Button size="small">
            <FavoriteIcon />
          </Button>
          <Button size="small">
            <VisibilityIcon />
          </Button>
        </ButtonGroup>
      </CardActions>
    </Card>
  );
};

const mapDispatchToProps = (dispatch) => {
  return {
    addToCart: (item, id) => dispatch(addToCart(item, id)),
    loadItem: (item) => dispatch(loadItem(item)),
  };
};

export default connect(null, mapDispatchToProps)(Product);
