import React, { useState, useEffect } from "react";
import Slider from "react-slick";
import styles from "../../styles/MainSlider.module.scss";
import Image from "next/image";
import SkeletonSlider from "../skeletonLoader/SkeletonSlider";

const SingleItem = (props) => {
  return (
    <div className={styles.slider__item}>
      <Image src={props.img} alt={props.name} layout="fill" />
    </div>
  );
};

const MainSlider = ({ sliderData }) => {
  const [data, setData] = useState(null);
  useEffect(() => {
    setTimeout(() => {
      setData(sliderData);
    }, 4000);
  }, []);

  const settings = {
    dots: false,
    infinite: true,
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1,
  };

  return (
    <div>
      {data && (
        <Slider {...settings}>
          {data.map((img) => (
            <SingleItem key={img.id} img={img.img} name={img.name} />
          ))}
        </Slider>
      )}

      {!data && <SkeletonSlider />}
    </div>
  );
};
export default MainSlider;
