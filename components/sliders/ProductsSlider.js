import React, { useEffect, useState } from "react";
//import "../../styles/CategoriesSlider.module.scss";
import Slider from "react-slick";
import Product from "../shop/Product";
import { connect } from "react-redux";
import SkeletonMulti from "../skeletonLoader/SkeletonMulti";

const ProductsSlider = ({ productsData }) => {
  const [data, setData] = useState(null);

  useEffect(() => {
    setTimeout(() => {
      setData(productsData);
    }, 4000);
  }, []);

  var animationStart = 0;
  var animationEnd = !data ? 0 : data.length;

  var arr = [];

  for (var i = animationStart; i < animationEnd; i++) {
    arr.push(i);
  }

  console.log(arr);

  const settings = {
    dots: false,
    infinite: true,
    speed: 500,
    slidesToShow: 3,
    slidesToScroll: 3,
    responsive: [
      {
        breakpoint: 1500,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 1,
          infinite: true,
          dots: true,
        },
      },
      {
        breakpoint: 1000,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
          infinite: true,
          dots: true,
        },
      },
      {
        breakpoint: 500,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
          initialSlide: 1,
        },
      },
    ],
  };
  return (
    <div>
      {data && (
        <Slider {...settings}>
          {data.map((product) => (
            <div>
              <Product
                image={product.image}
                name={product.name}
                key={product.id}
                price={product.price}
                id={product.id}
                animationDelay={arr}
                product={product}
              />
            </div>
          ))}
        </Slider>
      )}
      {!data && <SkeletonMulti />}
    </div>
  );
};

export default ProductsSlider;
