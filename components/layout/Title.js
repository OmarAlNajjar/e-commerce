import React, {  useEffect } from "react";


const Title = (props) => {

  useEffect(() => {

    // In order to make the effect on hover 
    const titleHover = document.querySelectorAll(".hover-this");
    const cursor = document.querySelector(".cursor");

    const animateit = function (e) {
      const h2 = this.querySelector(".title");
      const { offsetX: x, offsetY: y } = e,
        { offsetWidth: width, offsetHeight: height } = this,
        move = 20,
        xMove = (x / width) * (move * 2) - move,
        yMove = (y / height) * (move * 2) - move;

      h2.style.transform = `translate(${xMove}px, ${yMove}px)`;
      cursor.style.transform = `scale(8)`;
      e.path[0].style.transform = `scale(1.2)`;

      if (e.type === "mouseleave") {
        h2.style.transform = "";
        cursor.style.transform = ``;
        e.path[0].style.transform = ``;
      }
    };

    // In order to make the Cursor 
    const editCursor = (e) => {
      const { clientX: x, clientY: y } = e;
      cursor.style.left = x + "px";
      cursor.style.top = y + "px";
    };

    titleHover.forEach((b) => b.addEventListener("mousemove", animateit));
    titleHover.forEach((b) => b.addEventListener("mouseleave", animateit));
    window.addEventListener("mousemove", editCursor);
  }, []);

  return (
    <div className="titleParent">
      <div className="hover-this">
        <h2 className="title">{props.name} </h2>
      </div>
    </div>
  );
};

export default Title;
