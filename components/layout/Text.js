import React , {useEffect} from "react";
import { gsap } from "gsap";
import styles from '../../styles/Text.module.scss'
 
export default function Text(props) {
    // In order to make the text effect 
    useEffect(() => {
        const textAnimation = gsap.timeline();
        textAnimation.from(`.${styles.line} span` , 1.8 , {
            y : 200 , 
            wase : "powe4.out",
            skewY : 10 , 
            delay : 1,
            stagger : {
                amount : 0.4
            },
        })
    }, [])



 return (
   <div className={styles.wrapper}>
     <div className={styles.line}>
         <span>
            {props.text1}
         </span>
     </div>
     <div className={styles.line}>
         <span>
            {props.text2}
         </span>
     </div>

   </div>
 );
}