import * as React from "react";
import Grid from "@mui/material/Grid";
import Box from "@mui/material/Box";
import Container from "@mui/material/Container";
import styles from "../../styles/Footer.module.scss";
import AppleIcon from "@mui/icons-material/Apple";
import AndroidIcon from "@mui/icons-material/Android";
import InstagramIcon from "@mui/icons-material/Instagram";
import FacebookIcon from "@mui/icons-material/Facebook";
import TwitterIcon from "@mui/icons-material/Twitter";
import EmailIcon from "@mui/icons-material/Email";
import LocalPhoneIcon from "@mui/icons-material/LocalPhone";
import Link from "next/link";
const About = () => {
  return (
    <>
      <h2 className={styles.header}>about us</h2>
      <AppleIcon />
    </>
  );
};
const ContactUs = () => {
  return (
    <>
      <h2 className={styles.header}> contact us</h2>
      <ul>
        <li className={styles.flex__icon}>
          <EmailIcon />
          <span>
          email@email.com
          </span>
        </li>
        <li className={styles.flex__icon}>
          <LocalPhoneIcon />
          <span>
          +99999999999
          </span>

        </li>
      </ul>
    </>
  );
};

const Apps = () => {
  return (
    <>
      <h2 className={styles.header}> apps </h2>
      <ul>
        <li>
          <a href="" className={styles.flex__icon}>
            <AndroidIcon />
            <span>android</span>
          </a>
        </li>

        <li>
          <a href="" className={styles.flex__icon}>
            <AppleIcon />
            <span>ios</span>
          </a>
        </li>
      </ul>
    </>
  );
};
const SocialMedia = () => {
  return (
    <>
      <h2 className={styles.header}> follow us </h2>

      <ul className={styles.social__media__container}>
        <li className={styles.flex__icon}>
          <InstagramIcon /> <span> Instagram </span>
        </li>
        <li className={styles.flex__icon}>
          <FacebookIcon /> <span> facebook </span>
        </li>
        <li className={styles.flex__icon}>
          <TwitterIcon /> <span> twitter </span>
        </li>
      </ul>
    </>
  );
};

const Copyright = () => {
  return <div className={styles.copyright}>© Copyright by Omar</div>;
};

const Pages = () => {
  return (
    <Container maxWidth="lg">
      <Grid container rowSpacing={1} columnSpacing={{ xs: 1, sm: 2, md: 3 }}>
        <Grid item xs={12} md={6} lg={3}>
          <Link href="/terms" replace>
            <a>terms & conditions</a>
          </Link>
        </Grid>

        <Grid item xs={12} md={6} lg={3}>
          <Link href="/privacy" replace>
            <a>privacy policy</a>
          </Link>
        </Grid>

        <Grid item xs={12} md={6} lg={3}>
          <Link href="/events" replace>
            <a>events</a>
          </Link>
        </Grid>

        <Grid item xs={12} md={6} lg={3}>
          <Link href="/map" replace>
            <a>map</a>
          </Link>
        </Grid>
      </Grid>
    </Container>
  );
};

export default function RowAndColumnSpacing() {
  return (
    <footer>
      <div class="triangle-up"></div>
      <Box className={styles.footer}>
        <Container maxWidth="lg">
          <Grid
            container
            rowSpacing={1}
            columnSpacing={{ xs: 1, sm: 2, md: 3 }}
          >
            <Grid item xs={12} md={6} lg={3}>
              <About />
            </Grid>
            <Grid item xs={12} md={6} lg={3}>
              <ContactUs />
            </Grid>
            <Grid item xs={12} md={6} lg={3}>
              <SocialMedia />
            </Grid>
            <Grid item xs={12} md={6} lg={3}>
              <Apps />
            </Grid>
          </Grid>
          <Pages />
          <Copyright />
        </Container>
      </Box>
    </footer>
  );
}
