import React , {useEffect}from "react";
import "../styles/globals.css";
import Header from "../components/layout/Header";
import Footer from "../components/layout/Footer";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import { wrapper } from "../redux/store";
import "bootstrap/dist/css/bootstrap.min.css";

function MyApp({ Component, pageProps }) {

  // In order to add three js script on init 

  useEffect(() => {
    const threeScript = document.createElement("script");
    threeScript.setAttribute("id", "threeScript");
    threeScript.setAttribute(
      "src",
      "https://cdnjs.cloudflare.com/ajax/libs/three.js/r121/three.min.js"
    );
    document.getElementsByTagName("head")[0].appendChild(threeScript);
    return () => {
      if (threeScript) {
        threeScript.remove();
      }
    };
  }, []);


  return (
    <>
      <Header />
        <Component {...pageProps} />
      <Footer />
      {/* In order to make the cursar */}
      <div class="cursor"></div>
    </>
  );
}

export default wrapper.withRedux(MyApp);
