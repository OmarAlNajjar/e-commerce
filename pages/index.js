import Head from "next/head";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import ProductsSlider from "../components/sliders/ProductsSlider";
import Slider from "../components/sliders/Slider";
import Title from "../components/layout/Title";
import CategoryBar from "../components/shop/CategoryBar";
import Text from "../components/layout/Text";
import ServicesSection from "../components/shop/ServicesSection";
import { Container } from "@mui/material";

export default function Home({ sliderData, productsData }) {
  return (
    <div>
      <Head>
        <title>best e-commerce</title>
        <meta name="description" content="this is the best e-commerce shop " />
        <link rel="icon" href="/favicon.ico" />
        <link
          rel="stylesheet"
          type="text/css"
          charset="UTF-8"
          href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick.min.css"
        />
        <link
          rel="stylesheet"
          type="text/css"
          href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick-theme.min.css"
        />
        <meta name="keywords" content="shop, shopping, ecommerce" />
      </Head>
      <main>
        <Container>
          <Text
            text1="hello and welcome all your needs"
            text2="in one website . "
          />
          <Slider sliderData={sliderData} />
          <CategoryBar />

          <ProductsSlider productsData={productsData} />
          <Title name="our services" />
        </Container>
        <ServicesSection />
      </main>
    </div>
  );
}

export async function getStaticProps() {
  const ressliderData = await fetch("http://localhost:4000/slider");
  const sliderData = await ressliderData.json();

  const resproductsData = await fetch("http://localhost:4000/products");
  const productsData = await resproductsData.json();

  return {
    props: {
      sliderData,
      productsData,
    },
  };
}
