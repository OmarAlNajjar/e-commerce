import * as shoppingTypes from "./shoppingTypes";
import axios from "axios";

export const addToCart = (item , id ) => {
  return {
    type: shoppingTypes.ADD_TO_CART,
    payload: {
      id,
      item,
    },
  };
};

export const removeFromCart = (id) => {
  return {
    type: shoppingTypes.REMOVE_FROM_CART,
    payload: {
      id,
    },
  };
};

export const adjustQty = (id, qty) => {
  return {
    type: shoppingTypes.ADJUST_QTY,
    payload: {
      id,
      qty,
    },
  };
};

export const loadItem = (item) => {
  return {
    type: shoppingTypes.LOAD_ITEM,
    payload: item,
  };
};

export const fetchProducts = () => {
  return (dispatch) => {
    axios
      .get("http://localhost:4000/products")
      .then((res) => {
        const products = res.data;
        dispatch(fetchProductsSuccess(products));
      })
      .catch((err) => {
        const errMessage = err.messages;
        dispatch(fetchProductsFailure(errMessage));
      });
  };
};

export const fetchProductsRequest = () => {
  return {
    type: shoppingTypes.FETCH_PRODUCTS_REQUEST,
  };
};

export const fetchProductsSuccess = (products) => {
  return {
    type: shoppingTypes.FETCH_PRODUCTS_SUCCESS,
    payload: products,
  };
};

export const fetchProductsFailure = (error) => {
  return {
    type: shoppingTypes.FETCH_PRODUCTS_FAILURE,
    payload: error,
  };
};
