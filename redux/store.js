import { createStore, applyMiddleware, compose } from "redux"
import rootReducer from "./rootReducer";
import { composeWithDevTools } from "redux-devtools-extension";
import { createWrapper } from "next-redux-wrapper"
import thunk from 'redux-thunk'
const middleware = [thunk]


const store = () => createStore(rootReducer , composeWithDevTools(applyMiddleware(...middleware)));
export const wrapper = createWrapper(store)